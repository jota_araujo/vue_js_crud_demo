# Description:

A CRUD demo app using [Vue.js](http://emberjs.com/) and [Semantic-UI](http://semantic-ui.com/).

For the Ember.js version of the same app, see: (https://bitbucket.org/jota_araujo/ember2_simple_crud).

# Screenshots:

![Vue.js CRUD demo](https://bytebucket.org/jota_araujo/vue_js_crud_demo/raw/967d7b15ad143dc5b5e47daf7218cb03838ec126/screenshots/screenshot1.jpg)