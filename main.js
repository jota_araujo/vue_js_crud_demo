import Vue from 'vue';
import App from './App';
import '../node_modules/semantic-ui-css/semantic.min.css';
import VueRouter from 'vue-router';

Vue.use(VueRouter);
Vue.config.debug = true;

const router = new VueRouter();

/* eslint-disable no-unused-vars */
function findComponent(name) {
  /* eslint-disable func-names */
  return function (resolve) {
    /* eslint-disable prefer-template */
    /* eslint-disable global-require */
    require(['./components/' + name + '.vue'], resolve);
  };
}

router.map({
  '/products': {
    name: 'product_list',
    component: require('./components/ProductList.vue'),
  },
  '/product/new': {
    name: 'product_new',
    component: require('./components/ProductNew.vue'),
  },
  '/product/edit/:id': {
    name: 'product_edit',
    component: require('./components/ProductEdit.vue'),
  },
});

router.redirect({
  // redirect any navigation to /a to /b
  '/': '/products',

  // redirect any not-found route to home
  '*': '/products',
});
router.start(App, '#app');
