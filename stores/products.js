/* eslint-disable prefer-arrow-callback */
/* eslint-disable no-restricted-syntax */
export default {

  state: {
    products: [],
    brands: [],
  },

  init() {
    debugger;
    console.log('I am the init----');
    this.state.products = [];
    this.state.brands = [];
  },

  setProducts(data) {
    this.state.products = data;
  },

  setBrands(data) {
    this.state.brands = data;
  },

  getProductById(id) {
    let result;
    this.state.products.forEach(function fn(product) {
      const numericId = parseInt(id, 10);
      if (product.id === numericId) result = product;
    });
    return result;
  },
};
